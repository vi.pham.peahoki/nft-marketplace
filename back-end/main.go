package main

import (
	router "back-end/router"

	"github.com/kataras/iris/v12"
)

func main() {
	app := iris.New()

	router.InitRoute(app)

	app.Listen(":8080")
}
